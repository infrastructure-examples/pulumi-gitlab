"use strict";

let pulumi = require("@pulumi/pulumi");
let gitlab = require("@pulumi/gitlab");

let group = new gitlab.Group("infrastructure-examples", {
  name: "Infrastructure Examples",
  path: "infrastructure-examples",
  description: "This groups contains Infrastructure as Code examples",
  requestAccessEnabled: true,
});

let project = new gitlab.Project("gitlab-aws-example", {
  name: "GitLab AWS Example",
  path: "gitlab-aws-example",
  visibilityLevel: "public",
  namespaceId: group.id,
});

exports.projectId = project.id;
